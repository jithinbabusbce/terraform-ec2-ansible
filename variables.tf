variable "AWS_REGION" {
  description = "Region everything is executed in."
}

variable "vpc-cidr" {
  description = "VPC CIDR Block for security group"
}

variable "vpc_id" {
  description = "VPC CIDR Block for security group"
}

variable "ssh-ipaddress1" {
  description = "Ip address Allowed for ssh into the EC2 instance"
}


variable "env" {
  description = "ENV Name"
}


variable "subnet_id" {
  description = "Subnet ID for Ec2 instance creation"
}


variable "ec2-type" {
  description = "EC2 instance type"
}

variable "ec2-ami" {
  description = "Ami Name"
}

variable "NAME_OF_PRIVATE_KEY" {
  description = "Private key Name"
}

variable "NAME_OF_PUBLIC_KEY" {
  description = "Public key Name"
}







