#!/bin/bash

terraform destroy -var-file=terraform.tfvars -auto-approve
echo "[public]" > ansible/hosts