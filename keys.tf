## Key file for the server login
resource "aws_key_pair" "Key" {
  key_name   = var.NAME_OF_PRIVATE_KEY
  public_key = file(var.NAME_OF_PUBLIC_KEY)
}
