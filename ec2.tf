## Public Server
resource "aws_instance" "ec2-server" {
  ami           = var.ec2-ami
  instance_type = var.ec2-type

  tags = {
    Name = join("-", [var.env, "ec2-server"])
    env  = var.env
  }

  ## Root Volume
  root_block_device {
    volume_size           = 50
    volume_type           = "gp2"
    delete_on_termination = true
  }

  # the VPC subnet
  subnet_id = var.subnet_id

  # the security group
  vpc_security_group_ids = [aws_security_group.ec2-server.id]

  # the key
  key_name = var.NAME_OF_PRIVATE_KEY
}

#######Elastic IP Address
resource "aws_eip" "ec2-server-eip" {
  vpc = true
}

#### EIP association
resource "aws_eip_association" "ec2-server-eip_assoc" {
  instance_id   = aws_instance.ec2-server.id
  allocation_id = aws_eip.ec2-server-eip.id
}
