############################ EC2 Server Security Group ########################
resource "aws_security_group" "ec2-server" {
  vpc_id      = var.vpc_id
  name        = join("-", [var.env, "ec2-sg"])
  description = "Security group that allows needed ports and all egress traffic for EC2 Server"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "ssh port"
    cidr_blocks = [var.ssh-ipaddress1]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc-cidr]
  }

  tags = {
    Name = join("-", [var.env, "ec2-server"])
    env  = var.env
  }
}


