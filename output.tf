output "ec2_ips" {
  value = aws_eip.ec2-server-eip.public_ip
}
