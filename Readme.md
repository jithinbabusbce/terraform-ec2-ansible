# Steps to follow for template deployment

Note: Follow creation and deletion steps mentioned in the readme file

### Prerequisites
1. terraform
2. AWS authentication in terminal
   ``` aws configure ```


## Parameter file name 
### Replace all env variable in this file
   ``` terraform.tfvars  ```

### For Running and creating the resources
``` bash create.sh```

## Ansible host file
  Location: ansible/hosts


## For SSH into the server
``` ssh -i ec2key ubuntu@<IP> ```

## For destroying all resources created
``` bash destroy.sh ```


## Ansible 

#### Ansible file folder
``` ansible ```

### Ansible variable file
Replace all variable names in this file like docker compose version, file names etc
``` ansible/setup/vars/main.yml ```

### Ansible copy file location
Replace files like keys, crond files etc in this folder
```ansible/setup/files/ ```



