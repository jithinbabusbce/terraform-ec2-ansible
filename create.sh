#!/bin/bash
terraform init
terraform get -update
terraform apply -input=false -var-file=terraform.tfvars  -auto-approve ## terraform apply
bash ansible.sh  #### Writing host IP address to hosts file
sleep 3m  ### Wait till server is in running status
pushd ansible && ansible-playbook setup.yml -vv ### Ansible code execution