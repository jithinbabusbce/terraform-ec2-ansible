#!/bin/bash

terraform output > file.txt
grep -oE "(\b[0-9]{1,3}\.)(\b[0-9]{1,3}\.)(\b[0-9]{1,3}\.)(\b[0-9]{1,3})" file.txt > ip.txt

while read i; do
  count=`grep -ir $i ansible/hosts  | wc -l`
  if [[ $count -lt 1 ]]
   then
  echo "$i ansible_ssh_user=ubuntu ansible_ssh_private_key_file=../ec2key" >> ansible/hosts;
  fi
done <ip.txt
rm ip.txt file.txt